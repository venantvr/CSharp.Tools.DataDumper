# Tools.DataDumper

In order to illuminate this potential black box, which is the acceptance test, it is sometimes necessary to make dumps of certain elements of the model (entities, notifications, value objects, etc.), on an exploitable and readable by experts media.
These dumps can become complete traces that illustrate the nominal cases of scenarios.

Unlike XML or JSON, YAML is a very simplified format intended to be read by human beings.

It can also be used for logging, but in the case of acceptance tests, it allows to complement the Gherkin scenarios by intelligible traces and to capture states.